import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

import { TodoClass } from '../class/todoClass';

@Injectable()
export class TodoService {

  private lastId: number;
  private todos: TodoClass[] = [];

  constructor(public http: Http, public storage: Storage) {

    this.getIdForTodos().then((idTodo) => {
      this.lastId = 0;
      if (idTodo) {
        this.lastId = idTodo;
      }
    });
  }

  addTodo(todo: TodoClass): TodoService {
    if (!todo.id) {
      todo.id = this.lastId++;
    }

    this.todos.push(todo);

    this.saveIdForTodos();
    this.saveAllTodos();

    return this;
  }

  deleteTodoById(id: number): TodoClass[] {
    this.todos = this.todos.filter(todo => todo.id !== id);

    this.saveAllTodos();

    return this.todos;
  }

  updateTodoById(id: number, values: Object = {}): TodoClass {
    let myTodo = this.getTodoById(id);
    if (!myTodo) {
        return null;
    }
    Object.assign(myTodo, values);

    this.saveAllTodos();

    return myTodo;
  }

  toggleTodoComplete(todo: TodoClass): void {
    this.updateTodoById(todo.id, {
        complete : !todo.complete
    });

    this.saveAllTodos();
  }

  getTodoById(todoId: number): TodoClass {
    let todo = this.todos.find(Todo => Todo.id == todoId);
    return todo;
  }

  getAllTodos(): Promise<TodoClass[]> {
    return this.storage.get('todos').then((todos) => {
      this.todos = [];
      if (todos) {
        this.todos = JSON.parse(todos);
      }
      return this.todos;
    });
  }

  getIdForTodos(): Promise<number> {
    return this.storage.get('id_todo').then((idTodo) => {
      if (idTodo) {
        return JSON.parse(idTodo);
      }
      return null;
    });
  }

  saveAllTodos(): void {
    this.storage.set('todos', JSON.stringify(this.todos));
  }

  saveIdForTodos(): void {
    this.storage.set('id_todo', JSON.stringify(this.lastId));
  }
}
