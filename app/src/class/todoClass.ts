export class TodoClass {

  id: number;
  title: string = '';
  complete: boolean = false;

  constructor() { }
}