import { Component } from '@angular/core';

import { TodoClass } from '../../class/todoClass';

import { TodoService } from '../../providers/todo-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  styles: ["home.scss"],
  providers: [TodoService],
})
export class HomePage {

  newTodo: TodoClass = new TodoClass();
  todos: TodoClass[];

  constructor(public todoService: TodoService) {
  }

  ngOnInit() {
    this.todoService.getAllTodos().then((todos) => {
      this.todos = todos;
    });
  }

  addNewTodo(): void {
    if (this.newTodo.title === "") {
      return;
    }
    this.todoService.addTodo(this.newTodo);
    this.newTodo = new TodoClass();
  }

  handleCompleteTodo(todo: TodoClass): void {
    this.todoService.toggleTodoComplete(todo);
  }

  handleRemoveTodo(todo: TodoClass): void {
    this.todos = this.todoService.deleteTodoById(todo.id);
  }
}
