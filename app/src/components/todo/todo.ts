import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FabContainer } from 'ionic-angular';

import { TodoClass } from '../../class/todoClass';

@Component({
  selector: 'todo',
  templateUrl: 'todo.html',
  styles: ["todo.scss"],
})
export class Todo {

  @Input() todo: TodoClass;

  @Output() completeTodo: EventEmitter<TodoClass> = new EventEmitter();
  @Output() removeTodo: EventEmitter<TodoClass> = new EventEmitter();

  constructor() {
  }

  doCompleteTodo(): void {
    this.completeTodo.emit(this.todo);
  }

  doCompleteTodoWithFab(event: any, fab: FabContainer): void {
    this.completeTodo.emit(this.todo);
    fab.close();
  }

  doRemoveTodo(event: any, fab: FabContainer): void {
    this.removeTodo.emit(this.todo);
    fab.close();
  }
}
